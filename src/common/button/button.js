import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './button.scss';

const Button = props => {
  const { size, type, isLoading, isDisabled, width, onClick, label, icon } = props;
  return (
    <button
      type="button"
      className={classNames('btn', `btn_size_${size}`, `${type}_btn`, {
        loading_btn: isLoading,
        disabled_btn: isDisabled,
      })}
      disabled={isDisabled}
      style={{ width }}
      onClick={onClick || null}
    >
      {isLoading ? (
        <img className="loading_icon" src="/img/loading.svg" alt="loading" />
      ) : (
        <>
          {icon.show && <span className={`icon lnr-${icon.name}`} />}
          <span className="label">{label}</span>
        </>
      )}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.oneOf(['default', 'grey', 'blue', 'white']),
  isDisabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(['xs', 's', 'm', 'l', 'xl']),
  width: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  icon: PropTypes.shape({
    show: PropTypes.bool,
    name: PropTypes.string,
  }),
};

Button.defaultProps = {
  type: 'default',
  isDisabled: false,
  isLoading: false,
  onClick: () => {},
  size: 'm',
  width: 'auto',
  label: '',
  icon: {
    show: false,
    name: '',
  },
};

export default Button;
