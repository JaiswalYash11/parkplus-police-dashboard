import React from 'react';
import PropTypes from 'prop-types';
import { getRandomNum } from '../../utils/helpers';
import './inline_drop_down.scss';

const InlineDropDown = props => {
  const {
    label,
    options,
    value,
    placeholder,
    randomNum,
    width,
    onChange,
    name,
    hasError,
    errorText,
  } = props;
  return (
    <div className="inline_drop_down" style={{ width }}>
      {label !== '' && <label htmlFor={randomNum}>{label}</label>}
      <div className="input">
        <select id={randomNum} onChange={onChange} name={name} value={value}>
          <option value="" disabled="disabled" className="options_label">
            {placeholder}
          </option>
          {options.map(item => (
            <option key={item.value} value={item.value}>
              {item.name}
            </option>
          ))}
        </select>
        <span className="lnr lnr-chevron-down" />
      </div>
      {hasError && <div className="cmn_input_error error">{errorText}</div>}
    </div>
  );
};

InlineDropDown.propTypes = {
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({ name: PropTypes.string, value: PropTypes.string }))
    .isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  width: PropTypes.string,
  hasError: PropTypes.string,
  errorText: PropTypes.string,
  randomNum: PropTypes.number,
};

InlineDropDown.defaultProps = {
  placeholder: 'Select',
  label: '',
  value: '',
  width: '100%',
  hasError: false,
  errorText: 'Invalid',
  randomNum: getRandomNum(),
};

export default InlineDropDown;
