import React from 'react';
import PropTypes from 'prop-types';
import './detail_card.scss';

const DetailCard = props => {
  const { data, onClose, isResident } = props;
  debugger
  return (
    <div className="detail_card">
      {isResident && <div className="inner_card">
        <img src={data.image_url} alt="PHOTO" width={300} height={235}/>
        <div> Name : {data.name}</div>
        <div> Mobile : {data.mobile}</div>
      </div>}
      {!isResident && <div className="inner_card">
        <img src={data.image_url} alt="PHOTO" width={300} height={235}/>
        <div> Name : {data.name}</div>
        <div> Address : {data.address}</div>
      </div>}

      {/*{JSON.stringify(data)}*/}
      <button type="submit" onClick={onClose}>
        <span className="lnr-cross2" />
      </button>
    </div>
  );
};

DetailCard.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  onClose: PropTypes.func,
};

DetailCard.defaultProps = {
  onClose: () => {},
};

export default DetailCard;
