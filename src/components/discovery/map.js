import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import './map.scss';
import Marker from "./marker";
import DetailCard from "./detail_card";

const ZOOM =12;

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      zoom: ZOOM,
      center: {
        lat: 28.6139,
        lng: 77.2090
      },
      selectedCard:null,
    };
  }

  setSelectedCard = selectedCard => this.setState({ selectedCard });

  render(){
    const { zoom,center, selectedCard } = this.state;
    const { markers, isResident } = this.props;
    const markersComp = [];
    if (markers !== null) {
      for (let iter = 0; iter < markers.length; iter += 1) {
        const item = markers[iter];
        let isVerified =true;
        if (item.owners_verified === false && item.tenants_verified === false)
          isVerified=false;
        markersComp.push(
          <Marker
            key={item.id}
            lat={item.lat}
            lng={item.lng}
            data={item}
            isVerified={isVerified}
            onClick={() => this.setSelectedCard(item)}
          />
        );
      }
    }
    return(<div className="map_view">
      <GoogleMapReact
        bootstrapURLKeys={{key:'AIzaSyCP_cEINV9TSre8a3Eq1NDHikunqzd2lTQ'}}
        defaultCenter={center}
        center={center}
        defaultZoom={ZOOM}
        zoom={zoom}
        options={{
          zoomControl: true,
          scaleControl: true,
          fullscreenControl: false,
          mapTypeControl: false,
          rotateControl: false,
          myLocationControl: true,
          styles: [
            {
              stylers: [
                { saturation: -70 },
                { gamma: 1 },
                { lightness: 1 },
                { visibility: 'on' },
              ],
            },
          ],
        }}
        onZoomAnimationEnd={zoom => this.setState({ zoom })}
      >
        {markersComp}
        {/*<MarkerCurrentLoc lat={currentLoc.coords.lat} lng={currentLoc.coords.long} />*/}
      </GoogleMapReact>
      {selectedCard !== null && (
        <div className="marker_detail">
          <DetailCard data={selectedCard} onClose={() => this.setSelectedCard(null)} isResident={isResident} />
        </div>
      )}
    </div>)
  }
}

Map.propTypes = {
  markers: PropTypes.arrayOf(PropTypes.object),
};

Map.defaultProps = {
  markers: null,
};

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps)(Map)