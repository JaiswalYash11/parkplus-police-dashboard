import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import './marker.scss';

const Marker = props => {
  const {onClick, isVerified} = props;
  return (<div className={classNames({
      marker_green: isVerified === true,
      marker_red: isVerified === false,
    })} onClick={onClick}>
    </div>
  );
};

Marker.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  onClick: PropTypes.func,
};

Marker.defaultProps = {
  onClick: () => {
  },
};

export default Marker;

