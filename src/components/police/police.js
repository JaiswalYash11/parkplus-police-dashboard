import React, {Component} from 'react';
import {connect} from 'react-redux';
import {checkboxList} from '../../data/common'
import Map from "../discovery/map";
import './police.scss'
import {noCheckFetch, checkFetch, fetchThanas, fetchBeats} from '../../store/actions/actions';
import Button from "../../common/button/button";
import InlineDropDown from "../../common/dropdown/inline_drop_down";

const SORT_DROPDOWN = [
  { name: 'Booking Date', value: 'start_date' },
  { name: 'Check-in Date', value: 'check_in' },
  { name: 'Check-out Date', value: 'check_out' },
];
class Police extends Component {
  constructor(props) {
    super(props);
    this.state = {
      characters: {},
      val: false,
      checkboxVal: {
        bad_character: false,
        bail_release: false,
        active_criminal: false,
        parole: false,
        proclaimed_offender: false,
        wanted: false,
        ruffian: false,
        refugee: false,
        foreigner: false,
        building_criminal: false,
      },
      thanaName:'',
      beatName:'',
    };
  }

  componentDidMount() {
      const { fetchThanas } = this.props;
      fetchThanas();
  }

  onCheckboxChange = (name) => {
    const dummyCheckBox = this.state.checkboxVal;
    dummyCheckBox[name] = !dummyCheckBox[name];
    this.setState({checkboxVal: dummyCheckBox});
  };

  onhandleApplyClick = () => {
    const {checkboxVal,thanaName, beatName} = this.state;
    const { onCheckFetch, thanaList, beatList, onNoCheckFetch } =  this.props;
    let cluster_id= null;
    let branch_id =null;
    if (thanaName!==''){
      const id = this.findIdByValue(thanaName, thanaList);
      cluster_id = id;
    }
    if (beatName!==''){
      const id = this.findIdByValue(beatName, beatList);
      branch_id = id;
    }
    const finalObject = {}
    Object.entries(checkboxVal).forEach(val => {
      if (val[1] === true) {
        finalObject[val[0]] = 1;
      }
    });
    debugger
    if (Object.entries(finalObject).length !== 0) {
      onCheckFetch({characters: finalObject, cluster_id: cluster_id, branch_id:branch_id });
    } else onNoCheckFetch({cluster_id,branch_id})
  };

  findIdByValue = (val, collection) => {
    let id;
    for (let i=0; i<collection.length;i++){
      const key = (collection[i].name).toLowerCase().split(' ').join('_')
      if (key === val){
        id = collection[i].id;
        break;
      }
    }
    return id;
  };

  onInputChange = (name, value) => {
    const { fetchBeats, thanaList } = this.props;
    if (name === 'thanaName') {
      const id = this.findIdByValue(value, thanaList);
      this.setState({[name]: value, beatName:''}, ()=>{
        fetchBeats(id);
      })
    }
   else this.setState({[name]:value})
  };


  render() {
    const {checkboxVal, thanaName, beatName} = this.state;
    const { thanaList, beatList,residentList, buildingList } = this.props;
    const finalList = residentList ? residentList : buildingList;
    const dummyThanaList = []
    const dummyBeatList = []
    if (thanaList !==null){
      if (thanaList){
        thanaList.forEach(val => {
          dummyThanaList.push({
            name: val.name,
            value: val.name.toLowerCase().split(' ').join('_')
          })
        })
      }
    }
    if (beatList !==null){
      if (beatList){
        beatList.forEach(val => {
          dummyBeatList.push({
            name: val.name,
            value: val.name.toLowerCase().split(' ').join('_')
          })
        })
      }
    }
    return (<div className="body">
     <div className="top_head">
      <div className="logo"/>
      <div className="title">Parakh </div>
       <div className="sub_title"> Powered by PARKWHEELS</div>
     </div>
      <div className="header">
        <div className="left_header">
          {checkboxList.map(val => {
            return (<div>
              <input type="checkbox" id={val.id} name={val.name} checked={checkboxVal[`${val.key}`]}
                     onChange={() => this.onCheckboxChange(val.key)}/>
              <span> {val.name}</span>
            </div>)
          })}
        </div>
        <div className="right_header">
          <div className="item">
            <InlineDropDown
              name="thanaName"
              label="Thana Name"
              placeholder="Select"
              options={dummyThanaList!==null && dummyThanaList}
              onChange={e => this.onInputChange('thanaName', e.target.value)}
               value={thanaName}
              hasError=""
              errText=""
            />
          </div>
          <div className="item">
            <InlineDropDown
              name="beatName"
              label="Beat Name"
              placeholder="Select"
              options={dummyBeatList!==null && dummyBeatList}
              onChange={e => this.onInputChange('beatName', e.target.value)}
              value={beatName}
              hasError=""
              errText=""
            />
          </div>
          <div className="button_style">
            <Button
              type="default"
              label="Apply"
              icon={{ show: false, name: 'equalizer' }}
              size="s"
              width="80px"
              onClick={this.onhandleApplyClick}
            />
          </div>
        </div>
      </div>
      <Map markers={finalList!==null && finalList} isResident={residentList !== null}/>
    </div>)
  }
}

const mapStateToProps = (state) => ({
  residentList: state.residentList  ,
  buildingList: state.buildingList,
  beatList:state.beatList,
  thanaList: state.thanaList,
});

const mapDispatchToProps = dispatch => {
  return {
    onNoCheckFetch: (val) => dispatch(noCheckFetch(val)),
    onCheckFetch: (val) => dispatch(checkFetch(val)),
    fetchBeats: (val) => dispatch(fetchBeats(val)),
    fetchThanas: () => dispatch(fetchThanas())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Police);