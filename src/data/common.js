export const checkboxList = [
  {name: 'Bad Character', key: 'bad_character', id: 1},
  {name: 'Bail Release', key: 'bail_release', id: 2},
  {name: 'Active Criminal', key: 'active_criminal', id: 3},
  {name: 'Parole', key: 'parole', id: 4},
  {name: 'Proclaimed Offender', key: 'proclaimed_offender', id: 5},
  {name: 'Wanted', key: 'wanted', id: 6},
  {name: 'Ruffian', key: 'ruffian', id: 7},
  {name: 'Refugee', key: 'refugee', id: 8},
  {name: 'Foreigner', key: 'foreigner', id: 9},
  {name: 'Building Criminal', key: 'building_criminal', id: 10},
];