import {postRequest, getRequest} from "../../utils/api";
import axios from "axios";

export const noCheckFetchAsync = (data) =>{
  return{ type:'ON_NO_CHECK_FETCH', value:data}
};


export const noCheckFetch = (val) => {
  const url = `http://police-beta.myparkplus.com/v1/clusters/building_list?branch_id=${val.branch_id}&cluster_id=${val.cluster_id}`;
  return  dispatch => {
    axios.get(url)
      .then(resp => {
        dispatch(noCheckFetchAsync(resp.data.list));
      })
      .catch(error => {
        console.log('error.response', error.response);
        dispatch(noCheckFetchAsync(null));
      });
  }
};


//CHECK FETCH
export const checkFetchAsync = (data) =>{
  return{ type:'ON_CHECK_FETCH', value: data}
};

export const checkFetch = (val) =>{
  const url ='http://police-beta.myparkplus.com/v1/characters';
  return dispatch => {
    axios.post(url, val)
      .then(response => {
        debugger
        dispatch(checkFetchAsync(response.data.list))
      })
      .catch(error => {
        dispatch(checkFetchAsync(null))
      });
  }
};

//FETCH THANAS
export const fetchThanasAsync = (data) => {
  return { type: 'FETCH_THANAS', value: data}
}

export const fetchThanas =  () => {
  const url = 'http://police-beta.myparkplus.com/v1/clusters';
  return  dispatch => {
    axios.get(url)
      .then(resp => {
        dispatch(fetchThanasAsync(resp.data.list));
      })
      .catch(error => {
        console.log('error.response', error.response);
        dispatch(fetchThanasAsync(null));
      });
  }
};

//FETCH BEATS

export const getBeatsAsync = (data) => {
  return {type:'ON_GET_BEATS', value: data}
};

export const fetchBeats =  (id) => {
  const url = `http://police-beta.myparkplus.com/v1/clusters/beat?cluster_id=${id}`;
  return  dispatch => {
    axios.get(url)
      .then(resp => {
        dispatch(getBeatsAsync(resp.data.list));
      })
      .catch(error => {
        console.log('error.response', error.response);
        dispatch(getBeatsAsync(null));
      });
  }
};