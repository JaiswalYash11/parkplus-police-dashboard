const initialState = {
  residentList: null,
  thanaList:null,
  beatList:null,
};

const reducer = (state = initialState, action) => {
  const newState = {...state};

  switch(action.type) {
    case 'ON_CHECK_FETCH':
      newState.residentList = action.value;
      break;
    case 'ON_NO_CHECK_FETCH':
      newState.buildingList = action.value;
      break;
    case 'ON_GET_BEATS':
      newState.beatList = action.value;
      break;
    case 'FETCH_THANAS':
      newState.thanaList= action.value;
      break;
  }
  return newState;
};

export default reducer;