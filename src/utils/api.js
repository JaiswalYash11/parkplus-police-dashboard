import axios from 'axios';

export const postRequest = (url, reqPayload) => {
  axios.post(url, reqPayload)
    .then(response => {
      console.log(response);
      return response;
    })
    .catch(error => {
      console.log('error.response', error.response);
      return error;
    });
};

export const getRequest = (url) => {
  axios.get(url)
    .then(resp => {
      debugger
     return resp.data.list;
    })
    .catch(error => {
      console.log('error.response', error.response);
      return error;
    });
};